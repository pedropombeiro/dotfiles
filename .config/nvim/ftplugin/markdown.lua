-- Enable spellchecking
vim.opt_local.spell = true

-- Automatically wrap at 100 characters
vim.opt_local.textwidth   = 100
vim.opt_local.colorcolumn = "100"
